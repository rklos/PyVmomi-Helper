from setuptools import setup

setup(
    install_requires=[
        'pyVmomi'
    ],
    name='pyVmomiHelper',
    version='1.2',
    packages=['pyVmomiHelper']
)
